!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
CHANNEL_NAME=population

docker-compose -f docker-compose.yaml down

# remove old images
#docker ps -aq -f name=dev-* | xargs docker rm | true
sleep 10
docker rmi -f $(docker images dev-* -qa) | true

docker-compose -f docker-compose.yaml up -d

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=100

sleep ${FABRIC_START_TIMEOUT}

# ======================================================================================================
# Create the channel
# ======================================================================================================
docker exec -e "CORE_PEER_LOCALMSPID=MoiMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/moi.cambodia.com.kh/users/Admin@moi.cambodia.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.moi.cambodia.com.kh:7051" cli peer channel create -o orderer1.cambodia.com.kh:7050 -o orderer2.cambodia.com.kh:7050 -o orderer3.cambodia.com.kh:7050 -c population_registry -f /etc/hyperledger/configtx/channel.tx | true

# ======================================================================================================
# Join the channel 
# ======================================================================================================
# Join peer1.moi.cambodia.com.kh to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=MoiMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/moi.cambodia.com.kh/users/Admin@moi.cambodia.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.moi.cambodia.com.kh:7051" cli peer channel join -b $CHANNEL_NAME.block

# Join peer2.moi.cambodia.com.kh to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=MoiMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/moi.cambodia.com.kh/users/Admin@moi.cambodia.com.kh/msp" -e "CORE_PEER_ADDRESS=peer2.moi.cambodia.com.kh:7051" cli peer channel join -b $CHANNEL_NAME.block

# ========================================================================================================
# Update anchor peers
# ========================================================================================================
# Update  peer1.moi.cambodia.com.kh to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=MoiMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/moi.cambodia.com.kh/users/Admin@moi.cambodia.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.moi.cambodia.com.kh:7051" cli peer channel update -o orderer1.cambodia.com.kh:7050 -o orderer2.cambodia.com.kh:7050 -o orderer3.cambodia.com.kh:7050 -c population_registry -f /etc/hyperledger/configtx/MoiMSPanchors.tx
