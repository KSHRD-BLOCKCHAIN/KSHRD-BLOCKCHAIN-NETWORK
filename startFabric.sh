#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)
LANGUAGE=${1:-"golang"}
CC_SRC_PATH=github.com/kshrdsmartcontract/go

# launch network; create channel and join peer to channel
cd ./network
./start.sh

# ======================================================================================================
# Now launch the CLI container in order to install, instantiate chaincode
# ======================================================================================================
docker exec -e "CORE_PEER_LOCALMSPID=CooconMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp" cli peer chaincode install -n kshrdsmartcontract -v 1.0 -p "$CC_SRC_PATH" -l "$LANGUAGE"

docker exec -e "CORE_PEER_LOCALMSPID=CooconMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.coocon.kshrd.com.kh:7051" cli peer chaincode install -n kshrdsmartcontract -v 1.0 -p "$CC_SRC_PATH" -l "$LANGUAGE"

docker exec -e "CORE_PEER_LOCALMSPID=WebcashMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer0.webcash.kshrd.com.kh:7051" cli peer chaincode install -n kshrdsmartcontract -v 1.0 -p "$CC_SRC_PATH" -l "$LANGUAGE"

docker exec -e "CORE_PEER_LOCALMSPID=WebcashMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.webcash.kshrd.com.kh:7051" cli peer chaincode install -n kshrdsmartcontract -v 1.0 -p "$CC_SRC_PATH" -l "$LANGUAGE"

docker exec -e "CORE_PEER_LOCALMSPID=CooconMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer0.coocon.kshrd.com.kh:7051" cli peer chaincode instantiate -o orderer1.kshrd.com.kh:7050 -o orderer2.kshrd.com.kh:7050 -o orderer3.kshrd.com.kh:7050 -C mychannel -n kshrdsmartcontract -l "$LANGUAGE" -v 1.0 -c '{"Args":["init","a", "100", "b","200"]}' -P "OR ('CooconMSP.member','WebcashMSP.member')"
sleep 10

# ======================================================================================================
# Now launch the CLI container in order to invoke chaincode
# ======================================================================================================
# docker exec -e "CORE_PEER_LOCALMSPID=CooconMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer0.coocon.kshrd.com.kh:7051" cli peer chaincode invoke -o orderer1.kshrd.com.kh:7050 -o orderer2.kshrd.com.kh:7050 -o orderer3.kshrd.com.kh:7050 -C mychannel -n kshrdsmartcontract -c '{"function":"init","Args":[]}'
# docker exec -e "CORE_PEER_LOCALMSPID=CooconMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/coocon.kshrd.com.kh/users/Admin@coocon.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.coocon.kshrd.com.kh:7051" cli peer chaincode invoke -o orderer1.kshrd.com.kh:7050 -o orderer2.kshrd.com.kh:7050 -o orderer3.kshrd.com.kh:7050 -C mychannel -n kshrdsmartcontract -c '{"function":"init","Args":[]}'

# docker exec -e "CORE_PEER_LOCALMSPID=WebcashMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer0.webcash.kshrd.com.kh:7051" cli peer chaincode invoke -o orderer1.kshrd.com.kh:7050 -o orderer2.kshrd.com.kh:7050 -o orderer3.kshrd.com.kh:7050 -C mychannel -n kshrdsmartcontract -c '{"function":"init","Args":[]}'
# docker exec -e "CORE_PEER_LOCALMSPID=WebcashMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/webcash.kshrd.com.kh/users/Admin@webcash.kshrd.com.kh/msp" -e "CORE_PEER_ADDRESS=peer1.webcash.kshrd.com.kh:7051" cli peer chaincode invoke -o orderer1.kshrd.com.kh:7050 -o orderer2.kshrd.com.kh:7050 -o orderer3.kshrd.com.kh:7050 -C mychannel -n kshrdsmartcontract -c '{"function":"init","Args":[]}'

printf "\nTotal setup execution time : $(($(date +%s) - starttime)) secs ...\n\n\n"
