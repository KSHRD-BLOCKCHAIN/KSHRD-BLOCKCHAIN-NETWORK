package main

const (
	prefixPopulation = "POPULATION"
)

type Population struct {
	personalCode string `json:"personalCode"`
	name         string `json:"name"`
	dateOfBirth  string `json:"dateOfBirth"`
	sex          string `json:"sex"`
	naltionality string `json:"citizen"`
	birthOfPlace string `json:"birthOfPlace"`
	address      string `json:"address"`
	photo        string `json:"photo"`
}
