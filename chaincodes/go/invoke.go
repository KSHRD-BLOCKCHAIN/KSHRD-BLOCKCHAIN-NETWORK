package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// TODO: To Save Population
func savePopulation(APIStub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Printf("Saving a New Population")

	if len(args) != 7 {
		return shim.Error("TODO: savePopulation ==> personalCode, name, dateofBirth, sex, naltionality, birthOfPlace, address")
	}

	key, err := APIStub.CreateCompositeKey(prefixPopulation, []string{args[1]})

	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Printf("TxID ==> " + APIStub.GetTxID())

	population := Population{
		personalCode: args[1],
		name:         args[2],
		dateOfBirth:  args[3],
		sex:          args[4],
		naltionality: args[5],
		birthOfPlace: args[6],
		address:      args[7],
	}

	fmt.Printf("%#v", population)

	populationAsBytes, err := json.Marshal(population)

	err = APIStub.PutState(key, populationAsBytes)

	if err != nil {
		fmt.Printf("savePopulation is error")
	}

	fmt.Printf("Population has been saved")

	return shim.Success(populationAsBytes)
}

func queryPopulation(APIStub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Printf("Query Population by PersonalCode")
	if len(args) != 1 {
		return shim.Error("queryPopulation function needs the arguments: personalCode")
	}

	key, err := APIStub.CreateCompositeKey(prefixPopulation, []string{args[0]})

	if err != nil {
		return shim.Error(err.Error())
	}

	populationAsBytes, err := APIStub.GetState(key)

	return shim.Success(populationAsBytes)
}

func queryAllPopulations(APIStub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Printf("Query All Population...")

	resultsIterator, err := APIStub.GetStateByPartialCompositeKey(prefixPopulation, []string{})

	if err != nil {
		return shim.Error(err.Error())
	}

	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()

		if err != nil {
			return shim.Error(err.Error())
		}

		population := Population{}

		err = json.Unmarshal(kvResult.Value, &population)

		if err != nil {
			return shim.Error(err.Error())
		}

		if err != nil {
			return shim.Error(err.Error())
		}

		results = append(results, population)
	}

	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(resultsAsBytes)
}
