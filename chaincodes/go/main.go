package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("main")

type SmartContract struct {
}

var blockchainFunctions = map[string]func(shim.ChaincodeStubInterface, []string) peer.Response{
	"savePopulation":    savePopulation,
	"queryPopulation":   queryPopulation,
	"queryAllPoulation": queryAllPopulations,
}

func (s *SmartContract) Init(APIStub shim.ChaincodeStubInterface) peer.Response {
	fmt.Println("========================================= Init Smart Contract =====================================")
	return shim.Success(nil)
}

func (s *SmartContract) Invoke(APIStub shim.ChaincodeStubInterface) peer.Response {
	fmt.Println("========================================= Invoke Smart Contract ======================================")

	function, args := APIStub.GetFunctionAndParameters()

	if function == "init" {
		return s.Init(APIStub)
	}

	blockchainFunctions := blockchainFunctions[function]
	if blockchainFunctions == nil {
		return shim.Error("Invalid invoke function.")
	}
	return blockchainFunctions(APIStub, args)
}

func main() {
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
	fmt.Printf("SmartContract is starting successfully.")
}
